import { useState, useEffect } from 'react';

export const usePosition = () => {
    const [coors, setCoors] = useState({
        latitude:0,
        longitude:0
    })

     const errorHandler = (positionError) => {
         // eslint-disable-next-line default-case
         switch(Number(positionError.code)) {
             case 1:
                alert("Error: Permission Denied! " + positionError.message);
                break;

            case 2:
                alert("Error: Position Unavailable! " + positionError.message);
                break;

            case 3:
                alert("Error: Timeout!" + positionError.message);
                break;
         }
        }

    const getCoors = (response) => {
        setCoors({
            latitude:response.coords.latitude,
            longitude:response.coords.longitude
        })
         };

    useEffect(() => {
        navigator.geolocation.getCurrentPosition(getCoors,errorHandler)
    }, [])

    return coors
}
