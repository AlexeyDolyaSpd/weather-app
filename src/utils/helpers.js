import i18n from "./i18n";

export const createWeekArray=(data)=>{

     let arr = data.map(i => {
        i.dt_txt = new Date(i.dt_txt).toLocaleString((`${i18n.language}`), {month: "short", day:'numeric'} )
        return i
    })


    let newArr = Object.values(
        arr.reduce( (c, e) => {
          if (!c[e.dt_txt]) c[e.dt_txt] = e;
          return c;
        }, {})
      );
                  
    return newArr;
};
