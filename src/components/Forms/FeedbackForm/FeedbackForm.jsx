import axios from "axios";
import React from "react";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import "./style.css";

export const FeedbackForm = () => {
  const { register, handleSubmit,reset } = useForm();
  const {t} = useTranslation();

  const onSubmit = (data) => {
    axios
      .post(`https://jsonplaceholder.typicode.com/posts`, data)
      .then((response) => console.log(response))
      .catch((err) => console.error(err));

      reset()
  };
  return (
    <div className='feedBackFormWrapper'>
      <h1>{t("feedbackForm.title")}</h1>
      <form onSubmit={handleSubmit(onSubmit)}>
        <input type='text' placeholder='Fullname' {...register('fullName')} />
        <input type='email' placeholder='Email' {...register('email')} />
        <label>
          {t("feedbackForm.messageLabel")}
          <textarea rows='5' {...register('message')}></textarea>
        </label>
        <button type='submit'>{t("feedbackForm.btnText")}</button>
      </form>
    </div>
  );
};
