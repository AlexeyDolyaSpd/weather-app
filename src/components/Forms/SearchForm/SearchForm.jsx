import React from 'react';
import { useTranslation } from 'react-i18next';
import  './style.css';


export const SearchForm = ({changeCity}) => {
  const {t} = useTranslation();

  const handlerSearchSubmit = (e) => {
    e.preventDefault();
    let cityName = e.target.cityName.value.trim();
    if(cityName){
      changeCity(cityName);
    e.target.cityName.value = '';
    }
  }

  return (
    <div className='searchFormWrapper'> 
      <form onSubmit={(e) => handlerSearchSubmit(e)} className='form'>
        <input className='input' 
        type="text" 
        name='cityName' 
        placeholder={t("searchForm.placeholder")} />
        <button className={'button'} type='submit'>{t("searchForm.btnText")}</button>
      </form>
    </div>
  );
};
