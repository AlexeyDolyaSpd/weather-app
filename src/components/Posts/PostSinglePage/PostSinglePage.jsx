import React , {useEffect, useState} from 'react';
import { useParams } from 'react-router-dom';
import api from '../../../services/api';
import './style.css';

export const PostSinglePage = () => {
    const {id} = useParams();
    const [post , setPost] = useState(null);

    useEffect(() => {
        api.post(id)
        .then(data => setPost(data))
        .catch(err => console.error(err))
    },[id])
    
  return (
    <div className='postSinglePageWrapper'>
        <div className='imgWrapper'>
            <img src={post?.thumbnailUrl} alt='img' />
        </div>
        <div className='title'>{post?.title}</div>
    </div>
  );
};
