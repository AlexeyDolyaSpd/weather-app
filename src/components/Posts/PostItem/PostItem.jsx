import React from 'react';
import './style.css'

export const PostItem = ({title , url}) => {
  return (
    <div className='weatherPostItemWrapper'>
        <div className='title'>{title}</div>
       <div className="imgWrapper">
            <img src={url} alt="img" />
       </div>
    </div>
  );
};
