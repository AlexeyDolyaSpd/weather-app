import React, { useEffect, useState } from "react";
import { PostItem } from "../PostItem/PostItem";
import { Link } from "react-router-dom";
import api from "../../../services/api";
import "./style.css";

export const Posts = () => {
  const [posts, setPosts] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [isFetching, setIsFetching] = useState(true);
  const [totalCount, setTotalCount] = useState(0);

  useEffect(() => getPosts(), [isFetching]);
  useEffect(() => {
    document.addEventListener("scroll", handleScroll);
    return function () {
      document.removeEventListener("scroll", handleScroll);
    };
  }, [isFetching]);

  const getPosts = () => {
    if (isFetching) {
        api.posts(currentPage)
        .then((response) => {
          setTotalCount(Number(response.headers["x-total-count"]));
          return response.data;
        })
        .then((data) => {
          setPosts([...posts, ...data]);
          setCurrentPage((prev) => prev + 1);
        })
        .catch((err) => console.error(err))
        .finally(() => {
          setIsFetching(false);
        });
    }
  };

  const handleScroll = (e) => {
    let scroll =
      e.target.documentElement.scrollHeight -
      (e.target.documentElement.scrollTop + window.innerHeight);
    if (scroll < 100 && posts.length < (totalCount - 10)) {
      setIsFetching(true);
    }
  };

  return (
    <div className='weatherPostsWrapper'>
      {posts.map((post) => {
        return (
          <Link to={`/weather-posts/${post.id}`} key={post.id}>
              <PostItem
                title={post.title}
                url={post.thumbnailUrl}/>
          </Link>
        );
      })}
    </div>
  );
};
