import React from 'react';
import './style.css';

export const ErrorWindow = ({children}) => {
  return (
    <div className='errorWrapper'>
        <div className='message'>{children}</div>
    </div>
 );
};
