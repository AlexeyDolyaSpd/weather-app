import React from 'react';
import { NavLink } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import  '../../../utils/i18n';
import './style.css'

export const Navigation = () => {
  const {t} = useTranslation();
  return (
        <div className='navigationWrapper'>
            <NavLink  to='/'>{t("navigation.weather")}</NavLink>
            <NavLink  to='/weather-posts'>{t("navigation.posts")}</NavLink>
            <NavLink  to='/feedback-form'>{t("navigation.form")}</NavLink>
        </div>
  )
};
