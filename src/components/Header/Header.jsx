import React from 'react';
import { LanguageToogle } from './LanguageToogle/LanguageToogle';
import { Navigation } from './Navigation/Navigation';
import './style.css'

export const Header = () => {
  return (
    <div className='header'>
        <Navigation/>
        <LanguageToogle/>
    </div>
  );
};
