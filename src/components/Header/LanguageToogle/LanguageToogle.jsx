import React from 'react';
import i18n from '../../../utils/i18n';
import './style.css'

export const LanguageToogle = () => {
  const handleChangeLanguage = (lang) => {
    i18n.changeLanguage(lang)
  };

  return (
    <div className='languageWrapper'>
        <button onClick={() => handleChangeLanguage('ru')}>ru</button>
        <button onClick={() => handleChangeLanguage('en')}>en</button>
    </div>
  );
};
