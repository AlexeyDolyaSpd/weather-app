import React from 'react';
import { DisplaySmall } from '../DisplaySmall/DisplaySmall';
import './style.css'

export const WeekForecastList = ({daysArr}) => {
  return (
    <div className='listWrapper'>
        {
        daysArr.map((day, index) => {
          return <DisplaySmall
            temperature = {day?.main?.temp}
            description = {day?.weather[0]?.description}
            iconId = {day?.weather[0]?.icon}
            date = {day?.dt_txt}
            key={index}/>
        })
        }
    </div>
  );
}

