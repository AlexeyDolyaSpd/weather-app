import { t } from 'i18next';
import React from 'react';
import './style.css'

export const Display = ({
  temperature,
  temperatureFeelsLike,
  humidity,
  pressure,
  windSpeed,
  description,
  iconId 
    }) =>  {
  return (
    <div className='currentWeatherWrapper'>
      <span className='description'>{description}</span>
      <div className='mainInfo'>
        <div className='iconWrapper'>
          <img src={`https://openweathermap.org/img/wn/${iconId}@2x.png`} alt='weather-icon' />
        </div>
        <span className='temp'>{`${Math.round(temperature)}°C`}</span>
      </div>
      <div className='additionalInfo'>
        <div className='additionalInfoItem'>
          {t("weather.feelsLike")}: <span>{`${Math.round(temperatureFeelsLike)}°C`}</span>
        </div>
        <div className='additionalInfoItem'>
        {t("weather.humidity")}: <span>{`${humidity}%`}</span>
        </div>
        <div className='additionalInfoItem'>
        {t("weather.pressure")} <span>{`${pressure}hPa`}</span>
        </div>
        <div className='additionalInfoItem'>
        {t("weather.wind")}: <span>{`${windSpeed}м/с`}</span>
        </div>
      </div>
    </div>
  );
}
