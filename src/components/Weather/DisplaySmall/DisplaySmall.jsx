import React from 'react';
import './style.css';

export const DisplaySmall = ({
    temperature,
    description,
    iconId,
    date
}) => {
  return (
    <div className='weatherSmallWrapper'> 
        <div className="weatherSmallFlexContainer">
            <div className='date'>{date}</div>
            <div className='temp'>{`${Math.round(temperature)}°C`}</div>
        <div className='imgWrapper'>
                <img src={`https://openweathermap.org/img/wn/${iconId}@2x.png`} alt='weather-icon '/>
            </div> 
        </div>
        <div className='description'>{description}</div>
    </div>
  );
};
