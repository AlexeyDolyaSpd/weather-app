import React, { useEffect, useState } from "react";
import Moment from "react-moment";
import "moment/locale/ru";
import api from "../../../services/api";
import { Display } from "../Display/Display";
import { SearchForm } from "../../Forms/SearchForm/SearchForm";
import { WeekForecastList } from "../WeekForecastList/WeekForecastList";
import { ErrorWindow } from "../../ErrorWindow/ErrorWindow";
import { createWeekArray} from "../../../utils/helpers";
import { usePosition } from "../../../utils/customHooks/usePosition";
import { useTranslation } from "react-i18next";
import i18n from "../../../utils/i18n";
import "./style.css";

export const Dashboard = () => {

  const {t} = useTranslation()  
  const [location, setLocation] = useState('');
  const [daysArray, setDaysArray] = useState([]);
  const [isLocationCorrect, setIsLocationCorrect] = useState(true);
  const [currentWeather, setCurrentWeather] = useState({
    temperature: "",
    temperatureFeelsLike: "",
    humidity: "",
    pressure: "",
    windSpeed: "",
    description: "",
    iconId: "",
  });

  const {latitude, longitude} = usePosition();

  useEffect(() => {
      api.userCity(latitude,longitude)
      .then(data => data?.address?.City)
      .then(city => setLocation(city))
      .catch(err => console.error(err))

  },[latitude,longitude])
  
  useEffect(()=> {
      getCurrentWeather(location);
      getWeekForecast(location);
  },[location, i18n.language])

  const changeCity = (newCity) => {
    setLocation(newCity);
  };

    const getCurrentWeather = (city) => {
       api.currentWeatherByCity(city , i18n.language)
        .then(data => {
          setCurrentWeather({
            city: data.name,
            date: new Date(),
            temperature: data?.main?.temp,
            temperatureFeelsLike: data?.main?.feels_like,
            humidity: data?.main?.humidity,
            pressure: data?.main?.pressure,
            windSpeed: data?.wind?.speed,
            description: data?.weather[0]?.description,
            iconId: data?.weather[0]?.icon,
          });
          setIsLocationCorrect(true);
        })
        .catch((error) => {
          if (error.response.status === 404) {
            setIsLocationCorrect(false);
          }
          console.error(error);
        });
    }


  const getWeekForecast = (city) => {
      api.weekForecastByCity(city, i18n.language)
        .then(data => data.list)
        .then(list => {
          setDaysArray(createWeekArray(list));
        })
        .catch((error) => console.error(error));
  };

  return (
    <div className='appWrapper'>
      <SearchForm changeCity={changeCity} />

      {
        (!isLocationCorrect)
        ? <ErrorWindow>{t("cityError")}</ErrorWindow>
        : <></>
      }

      <div className='forecastWrapper'>
        <div className='date'>
          <Moment locale={i18n.language} format="D MMM">{currentWeather.date}</Moment>
        </div>
        <div className='location'>{currentWeather.city}</div>
        <Display
            temperature={currentWeather.temperature}
            temperatureFeelsLike={currentWeather.temperatureFeelsLike}
            humidity={currentWeather.humidity}
            pressure={currentWeather.pressure}
            windSpeed={currentWeather.windSpeed}
            description={currentWeather.description}
            iconId={currentWeather.iconId}
        />
        <WeekForecastList daysArr={daysArray} />
      </div>
    </div>
  );
};
