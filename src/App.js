import {React, Suspense} from "react";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Dashboard } from "./components/Weather/Dashboard/Dashboard";
import { Posts } from "./components/Posts/Posts/Posts";
import { FeedbackForm } from "./components/Forms/FeedbackForm/FeedbackForm";
import { PostSinglePage } from "./components/Posts/PostSinglePage/PostSinglePage";
import { Header } from "./components/Header/Header";

function App() {
  return (
    <BrowserRouter>
    <Suspense fallback="loading">
      <Header />
      <Routes>
        <Route path="/" element={<Dashboard />} />
        <Route path="/weather-posts" element={<Posts />} />
          <Route path='/weather-posts/:id' element={<PostSinglePage/>}/>
        <Route path="/feedback-form" element={<FeedbackForm />} />
      </Routes>
      </Suspense>
    </BrowserRouter>
  );
}

export default App;
