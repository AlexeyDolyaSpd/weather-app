import axios from "axios";

const BASE_URL = 'https://api.openweathermap.org/data/2.5/';
const API_KEY = 'c2ecf2f3302d27c9071946c63ada0569';
const api = {
    async currentWeatherByCity(city, lang) {
       return  axios.get(`${BASE_URL}weather?q=${city}&units=metric&lang=${lang}&APPID=${API_KEY}`)
                    .then(response => response.data)
   },    

    async weekForecastByCity(city, lang) {
        return  axios.get(`${BASE_URL}forecast?q=${city}&units=metric&lang=${lang}&APPID=${API_KEY}`)
                     .then(response => response.data)
    },

    async userCity(latitude, longitude) {
        return axios.get(`https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/reverseGeocode?location=${longitude},${latitude}&f=json&langCode=en`)
                    .then(response => response.data)
    },

    async posts(currentPage) {
        return  axios.get(`https://jsonplaceholder.typicode.com/photos?_limit=10&_page=${currentPage}`)
    },

    async post(id){
        return axios.get(`https://jsonplaceholder.typicode.com/photos/${id}`)
                    .then(response => response.data)
    }

}

export default api;
